doAsearch () {
    echo -------------------------------------------------------------------------------------------------------------------
    echo New search. Enter the search terms [word1 and word2]:
    echo word1:
    read word1
    echo word2:
    read word2
    regExSearchString="$word1.*$word2|$word2.*$word1"

    echo Printing all the lines than contain [$word1] and [$word2].
    echo 
    grep --color=always -Hrn -i -E $regExSearchString --include *.txt

    doAsearch
}

doAsearch
