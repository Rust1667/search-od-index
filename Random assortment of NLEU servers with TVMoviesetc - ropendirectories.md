---
created: 2023-03-16T01:33:46 (UTC +01:00)
tags: []
source: https://libreddit.nl/r/opendirectories/comments/11sbkb9/random_assortment_of_nleu_servers_with_tvmoviesetc/
author: u/mingaminga
---

# Random assortment of NL/EU servers with TV/Movies/etc - r/opendirectories

> ## Excerpt
> View on Libreddit, an alternative private front-end to Reddit.

---
[r/opendirectories](https://libreddit.nl/r/opendirectories) • [u/mingaminga](https://libreddit.nl/user/mingaminga) • 2h ago

# Random assortment of NL/EU servers with TV/Movies/etc [TV](https://libreddit.nl/r/opendirectories/search?q=flair_name%3A%22TV%22&restrict_sr=on)

[https://leone74.thoon.feralhosting.com/torrent/](https://leone74.thoon.feralhosting.com/torrent/)

[http://185.148.0.210:9000/www/public/](http://185.148.0.210:9000/www/public/)

[https://noobftp2.noobsubs.com/](https://noobftp2.noobsubs.com/)

[https://storage1.nya.network/](https://storage1.nya.network/)

[http://185.148.0.210:9000/www/public/](http://185.148.0.210:9000/www/public/)

[https://geekz0ne.fr/data/geekz0ne/](https://geekz0ne.fr/data/geekz0ne/) (French)

[https://titoki.net/files/](https://titoki.net/files/)

[http://bloodmaker.anax.feralhosting.com/download/antho/](http://bloodmaker.anax.feralhosting.com/download/antho/)

[https://kingof2520s.titan.usbx.me/files/](https://kingof2520s.titan.usbx.me/files/)

[http://37.143.52.250/Series/](http://37.143.52.250/Series/)

[http://37.143.52.250/Manga/](http://37.143.52.250/Manga/)

[http://185.21.217.57:8000/?sort=modified&order=desc](http://185.21.217.57:8000/?sort=modified&order=desc)

[https://163.172.218.88/](https://163.172.218.88/)
