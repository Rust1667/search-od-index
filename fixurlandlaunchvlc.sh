# This script will fix URLs that have spaces and/or parenthesis and/or squarebrackets
# This is necessary for pasting URLs into the command line for VLC, to stream video from open directories

# Use the script like this:    ./fixurl.sh 'url with spaces and ()'
# The result would be --> FIXED URL: url%20with%20spaces%20and%20%28%29

urlInput=$1
string=$(echo $urlInput | sed 's/(/%28/g' | sed 's/)/%29/g' | sed 's/ /%20/g' | sed 's/\[/%5B/g' | sed 's/\]/%5D/g')

echo ------------------------------------
echo FIXED URL:  $string

vlc $string
